import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

   // TODO!!! Your fields here!

   private final double a1;

   private final double b1;

   private final double c1;

   private final double d1;

   static final double konstant = 1e-9;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      // TODO!!! Your constructor here!
      this.a1 = a;
      this.b1 = b;
      this.c1 = c;
      this.d1 = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return a1; // TODO!!!
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return b1; // TODO!!!
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return c1; // TODO!!!
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return d1; // TODO!!!
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      String answerString = "";
      answerString += a1;
      if (b1 - 0 > konstant || b1 == 0.0){
         if (b1 != 0.0){
            answerString += "+" + b1 + "i";
         }
      }
      else {
         answerString += b1 + "i";
      }
      if (c1 - 0 > konstant || c1 == 0.0){
         if (c1 != 0.0){
            answerString += "+" + c1 + "j";
         }
      }
      else {
         answerString += c1 + "j";
      }if (d1 - 0 > konstant || d1 == 0.0){
         if (d1 != 0.0){
            answerString += "+" + d1 + "k";
         }
      }
      else {
         answerString += d1 + "k";
      }
      return answerString;
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      try {
         double a1 = 0.0;
         double b1 = 0.0;
         double c1 = 0.0;
         double d1 = 0.0;
         String[] elements = new String[0];
         if (s.contains("i") ||s.contains("j") || s.contains("k")){
            elements = s.split("[ijk]");
         }
         else {
            a1 = Double.parseDouble(s);
            return new Quaternion(a1, b1, c1, d1);
         }
         for (int i = 0; i < 7; i++) {
            if (i == 0){
               String a1String = "";
               String b1String = "";
               String[] splt = elements[i].split("");
               int sign = 3;
               for (String s1 : splt) {
                  if (sign == 3) {
                     if (s1.equals("-")){
                        a1String += s1;
                     }
                     sign -= 1;
                  }
                  else if (s1.equals("-") && sign == 2 || s1.equals("+") && sign == 2){
                     b1String += s1;
                     sign -= 1;
                  }
                  if (!s1.equals("-") && sign == 2 && !s1.equals("+")){
                     a1String += s1;
                  }
                  else if (!s1.equals("-") && sign == 1 && !s1.equals("+")){
                     b1String += s1;
                  }
               }
               a1 = Double.parseDouble(a1String);
               b1 = Double.parseDouble(b1String);
            }
            if (i == 1){
               c1 = Double.parseDouble(elements[1]);
            }
            if (i == 2){
               d1 = Double.parseDouble(elements[2]);
            }

         }
         return new Quaternion(a1, b1, c1, d1);

      }catch (Exception e){
         throw new IllegalArgumentException("string " + s + " does not represent a quaternion");
      }
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(a1, b1, c1, d1);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(a1) < konstant && Math.abs(b1) < konstant &&
              Math.abs(c1) < konstant && Math.abs(d1) < konstant; // TODO!!!
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(a1, -b1, -c1, -d1); // TODO!!!
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      double aOpposite = -a1;
      double bOpposite = -b1;
      double cOpposite = -c1;
      double dOpposite = -d1;
      return new Quaternion(aOpposite, bOpposite, cOpposite, dOpposite); // TODO!!!
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      double asum = a1 + q.a1;
      double bsum = b1 + q.b1;
      double csum = c1 + q.c1;
      double dsum = d1 + q.d1;
      return new Quaternion(asum, bsum, csum, dsum); // TODO!!!
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double firstExpression = (a1 * q.a1 - b1 * q.b1 - c1 * q.c1 - d1 * q.d1);
      double secondExpression = (a1 * q.b1 + b1 * q.a1 + c1 * q.d1 - d1 * q.c1);
      double thirdExpression = (a1 * q.c1 - b1 * q.d1 + c1 * q.a1 + d1 * q.b1);
      double fourthExpression = (a1 * q.d1 + b1 * q.c1 - c1 * q.b1 + d1 * q.a1);
      return new Quaternion(firstExpression, secondExpression, thirdExpression, fourthExpression); // TODO!!!
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      double firstExpression = a1 * r;
      double secondExpression = b1 * r;
      double thirdExpression = c1 * r;
      double fourthExpression = d1 * r;
      return new Quaternion(firstExpression, secondExpression, thirdExpression, fourthExpression); // TODO!!!
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (isZero()) {
         throw new RuntimeException("Zero quaternion can not be inversed");
      }
      double v = a1 * a1 + b1 * b1 + c1 * c1 + d1 * d1;
      double firstExpression = ((a1) / v);
      double secondExpression = ((-b1) / v);
      double thirdExpression = ((-c1) / v);
      double fourthExpression = ((-d1) / v);
      return new Quaternion(firstExpression, secondExpression, thirdExpression, fourthExpression); // TODO!!!
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      double adif = a1 - q.a1;
      double bdif = b1 - q.b1;
      double cdif = c1 - q.c1;
      double ddif = d1 - q.d1;
      return new Quaternion(adif, bdif, cdif, ddif); // TODO!!!
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()){
         throw new RuntimeException("Quaternion should not be zero in divideByRight");
      }
      Quaternion myObj = this;
      Quaternion inversedQ = q.inverse();
      Quaternion answer = myObj.times(inversedQ);
      return answer; // TODO!!!
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()){
         throw new RuntimeException("Quaternion should not be zero in divideByLeft");
      }
      Quaternion myObj = this;
      Quaternion inversedQ = q.inverse();
      Quaternion answer = inversedQ.times(myObj);
      return answer; // TODO!!!
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if (!(qo instanceof Quaternion)){
         return false;
      }
      if (this == qo) {
         return true;
      }
      int counter = 0;
      Quaternion obj = (Quaternion) qo;
      if (Math.abs(obj.a1 - a1) < konstant){
         counter++;
      }
      if (Math.abs(obj.b1 - b1) < konstant){
         counter++;
      }
      if (Math.abs(obj.c1 - c1) < konstant){
         counter++;
      }
      if (Math.abs(obj.d1 - d1) < konstant){
         counter++;
      }
      return counter == 4;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      Quaternion obj = this;
      Quaternion qAfterCon = obj.times(q.conjugate());
      Quaternion pAfterCon = q.times(obj.conjugate());
      Quaternion lol = qAfterCon.plus(pAfterCon);
      Quaternion kek = lol.times(0.5);
      return kek; // TODO!!!
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      int hash = 37;
      hash = 37 * hash + Double.hashCode(a1);
      hash = 37 * hash + Double.hashCode(b1);
      hash = 37 * hash + Double.hashCode(c1);
      hash = 37 * hash + Double.hashCode(d1);
      return hash;
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(a1 * a1 + b1 * b1 + c1 * c1 + d1 * d1); // TODO!!!
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., -1, -2., 0.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
         System.out.println(arv1);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString()); //TODO: error now here
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
